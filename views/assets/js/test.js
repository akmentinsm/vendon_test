
$(document).ready(function() {
// Get total questions and set current to first
var total_questions = $('.questions').length;
var current_question = 0;

	// Hide all but first question
	$questions = $('.questions');
	$questions.hide();
	$($questions.get(current_question)).fadeIn();

	// When user answers the question, submit, hide current and show next
	$('.next').click(function(e){
		e.preventDefault();
		$($questions.get(current_question)).fadeOut(function(){

			// Keep track of completion progress
			var correct = 0;
			var current_count = current_question + 1;
			var progress = (current_count / total_questions) * 100;
			$("#bar").width(progress.toFixed() + '%');
			$("#progress-label").text(progress.toFixed() + '%');

			// Append hidden input with selected answer id
			$('<input>').attr({
				type: 'hidden',
				name: 'answer_id',
				value: e.target.id
			}).appendTo(this);

			var url = $(this).attr('action');

			// Post to server
			$.ajax({
				type: "POST",
				url: url,
				data: $(this).serialize(),
				error: function (data) {
					console.log('error');
					console.log(data);
				},

			});
			
			current_question++;

			if(current_question == total_questions){
				window.location.href = "../views/finish.view.php";
			}else{
				$($questions.get(current_question)).fadeIn();
			}
		});
	});
});