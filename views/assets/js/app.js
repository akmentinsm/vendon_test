function validateTestSelectForm() {

	// If an error message has already been displayed, remove it to prevent many messages from stacking
	if(document.getElementById("alert")){
		document.getElementById("alert").remove();
	}

	var nameError = document.getElementById("name-error");
	var name = document.getElementById("name").value; //Get name field value
	var para = document.createElement("p");
	var test = document.getElementById("test");
	var testError = document.getElementById("test-error");
	var testValue = test.options[test.selectedIndex].value; //Get selected option value

	// Validate fields
	if(name === ''){
		para.setAttribute("class","alert");
		var err = document.createTextNode("*Lūdzu, ievadiet vārdu");
		para.appendChild(err);
		nameError.appendChild(para);
		return false;
	}
	else if(name.length > 255){
		para.setAttribute("class","alert");
		var err = document.createTextNode("*Lūdzu, ievadiet vārdu, kas nepārsniedz 255 rakstzīmes!");
		para.appendChild(err);
		nameError.appendChild(para);
		return false;
	}
	else if(testValue == "" || testValue == 0){
		para.setAttribute("class","alert");
		var err = document.createTextNode("*Lūdzu, izvēlieties testu!");
		para.appendChild(err);
		testError.appendChild(para);
		return false;
	}
	else return true;
}
