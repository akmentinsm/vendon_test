<?php include "../inc/FinishController.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "../views/header.php" ?>
</head>
<body>
	<div class="container">
		<h1>Paldies, <?php echo $user_name ?></h1>
		<h3>Tu atbildēji pareizi uz <?php echo $correct_count ?> no <?php echo $question_count ?> jautājumiem!</h3>
		<a href="../" class="btn btn-primary btn-sm">Atpakaļ uz sākumlapu</a>
	</div>
</body>