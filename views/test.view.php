<?php include "../db_config.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "../views/header.php" ?>
	<script src="../views/assets/js/test.js"></script>
</head>
<body>
	<div class="container">
		<div id="progress-bar">
			<div id="bar">
				<div id="progress-label">&nbsp;0%</div>
			</div>
		</div>
		<?php include "../inc/QuestionController.php" ?>
	</div>
</body>
</html>