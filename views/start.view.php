
<h2>Testa uzdevums</h2>

<form class="form-horizontal" action='inc/UserController.php' onsubmit="return validateTestSelectForm()" method="post">
	<div class="form-group">
		<div class="col-sm-10">
			<input type="name" class="form-control" id="name" placeholder="Ievadi savu vārdu" name="name">
			<span class="error" id="name-error"><?php if(isset($_GET['err1'])) echo $_GET['err1']; ?></span>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10">
		<select id="test" name="test" class="form-control">
			<option value="0">Izvēlies testu</option>
			<?php
				while ($row = mysqli_fetch_assoc($query)) {
					echo "<option value='" . $row['id'] ."'>" . $row['name'] ."  </option>";
				}
			?>
		</select>
		<span class="error" id="test-error"><?php if(isset($_GET['err2'])) echo $_GET['err2']; ?></span>
	</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input id="submit" type="submit" class="btn btn-success" value="Sākt"></input>
		</div>
	</div>
</form>