<?php
include "../models/User.php";

$name = $test = "";
$testErr = $nameErr = "";

// Validate and store data when form is posted
if ($_SERVER["REQUEST_METHOD"] == "POST"){
  	//Data validation
  	$name = test_input($_POST["name"]);
  	validateName($name);
  	$test = test_input($_POST["test"]);
  	validateTest($test);

  	//If validation has passed
  	//Create new object of class User
  	$user = new User();
  	$user->add_user($name);
  	$user_id = $user->get_user_id();

  	//Store user and test id's in a session variables
  	session_start();
  	$_SESSION['test_id'] = $test;
  	$_SESSION['user_id'] = $user_id;
  	header('Location: ../views/test.view.php'); 
}

function test_input($data){
  	$data = trim($data); //will strip unnecessary characters from the user input data
  	$data = stripslashes($data); //will remove backslashes from the user input data
  	$data = htmlspecialchars($data); //converts special characters to HTML entities
  	return $data;
}

function validateName($data){
	if (!preg_match("/^[a-zA-Z ]*$/",$data)) {
		$nameErr = "Lūdzu, ievadiet tikai burtus!";

	} else if(strlen($data) > 255){
		$nameErr .= "Ievadītais vārds ir pārāk garš (255)!";

	} else if(empty($data)){
		$nameErr = "Lūdzu, ievadiet vārdu!";
	}
	if($nameErr){
		header('Location: ../?err1='.urlencode($nameErr));
		die;
	}
}

function validateTest($data){
	if($data == 0 || $data == ""){
		$testErr = "Lūdzu, izvēlieties testu";
		header('Location: ../?err2='.urlencode($testErr));
		die;
	}
	else return true;
}

?>