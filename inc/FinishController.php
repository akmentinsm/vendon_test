<?php
include "../db_config.php";

//Connect to database
$conn = new mysqli(servername, username, password, dbname);
if(mysqli_connect_errno()){
	echo "Error: Could not connect to database.";
	exit;
}
mysqli_set_charset($conn,"utf8");
session_start();

$user_id = $_SESSION['user_id'];
$test_id = $_SESSION['test_id'];

$user_name = getUserName($user_id, $conn);

// Correct answer count
if(!isset($correct_count)) $correct_count = 0;

// Fetch all user answers and compare them to correct_answer column in questions table to determine whether it is correct
$user_answer_query = mysqli_query($conn, "SELECT * FROM user_answers WHERE user_id = $user_id AND test_id = $test_id");
while ($row = mysqli_fetch_assoc($user_answer_query)) {
	$answer_id = $row['answer_id'];
	$correct_answers_query = mysqli_query($conn, "SELECT * FROM question_answers WHERE id = $answer_id");
	while ($answer_row = mysqli_fetch_assoc($correct_answers_query)) {
		if($answer_row['correct_answer'] == 1) $correct_count++;
	}
}

// Fetch total question count
$questions_query = mysqli_query($conn, "SELECT * FROM questions WHERE test_id = $test_id");
$question_count = mysqli_num_rows($questions_query);

// Fetch users name from database
function getUserName($user_id, $conn){
	$query = mysqli_query($conn, "SELECT * FROM users WHERE id = $user_id");
	while ($row = mysqli_fetch_assoc($query)) {
		return $row['name'];
	}
}

?>