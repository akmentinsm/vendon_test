<?php
include "db_config.php";

session_start();
session_unset();

//Connect to database
$conn = new mysqli(servername, username, password, dbname);
if(mysqli_connect_errno()){
	echo "Error: Could not connect to database.";
	exit;
}
mysqli_set_charset($conn,"utf8");

//Select all tests
$query = mysqli_query($conn, "SELECT * FROM tests");

require 'views/start.view.php';
?>