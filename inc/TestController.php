<?php
include "../models/Answer.php";

// Connect to database
$conn = new mysqli(servername, username, password, dbname);
if(mysqli_connect_errno()){
	echo "Error: Could not connect to database.";
	exit;
}
mysqli_set_charset($conn,"utf8");

session_start();

// Store answer each time form has been submitted
if ($_SERVER["REQUEST_METHOD"] == "POST"){
	$user_id = $_SESSION['user_id'];
	$test_id = $_SESSION['test_id'];
	$question_id = $_POST["question_id"];
	$answer_id = $_POST["answer_id"];

	$answer = new Answer();
	$answer->add_answer($user_id, $test_id, $question_id, $answer_id);
}

?>