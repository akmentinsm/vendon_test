<?php
//Connect to database
$conn = new mysqli(servername, username, password, dbname);
if(mysqli_connect_errno()){
	echo "Error: Could not connect to database.";
	exit;
}
mysqli_set_charset($conn,"utf8");

session_start();
$test_id = $_SESSION['test_id'];

// Fetch all questions for the test
$questions_query = mysqli_query($conn, "SELECT * FROM questions WHERE test_id = $test_id");
while($row = mysqli_fetch_assoc($questions_query)){
	// Store questions in array
	$questions[] = $row;
}

// Loop through all questions displaying all answers for each question
foreach ($questions as $question) {
	$question_id = $question['id'];

	// Create new form for each question
	echo "<form class='questions' id='".$question_id."' action='../inc/TestController.php'  method='post'>";
	echo " <strong>" . $question['question'] . "</strong><br>";

	// Fetch all answers for the question
	$answers_query = mysqli_query($conn, "SELECT * FROM question_answers WHERE question_id = $question_id");

	// Get row count in order to determine if div closing tag will be needed for row
	// There can be only 2 columns per row
	$row_count = mysqli_num_rows($answers_query);
	$current_rows = 0;

	echo '<div class="row">';
	while($row = mysqli_fetch_assoc($answers_query)){
		static $counter=0; //Variable to determine which column is currently used

		// Start new row if counter is set to 0
		if($counter==0) echo '<div class="row">';
		
		// Display each answer as clickable button with hidden question_id input field
		echo '
		<div class="col-xs-6 form-group answer-'.$row['id'].'">'
		.'<button type="submit" class="btn btn-primary select_answer_btn next" id="'.$row['id'].'">'.$row["answer"].'</button>
		<input type="hidden" name="question_id" value="'.$question_id.'" />
		</div>
		';

		$counter++;
		$current_rows++;
		
		if($counter==2){ echo '</div>'; $counter=0;}
		if($counter!=2 && $current_rows == $row_count){ echo '</div>'; $counter=0;}
	}
	echo '</div>';
	echo "</form>";
}

?>