-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2018 at 08:23 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vendon_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `test_id`, `question`) VALUES
(1, 1, 'Cik ir 2+2?'),
(2, 1, 'Cik ir 5+2?'),
(3, 1, 'Cik ir 10-5?'),
(4, 1, 'Cik ir 50+50?'),
(5, 2, 'Kas ir Latvijas galvaspilsēta?'),
(6, 2, 'Kas ir Latvijas lielākā pilsēta?'),
(7, 2, 'Kura ir mazākā pilsēta Latvijā?'),
(8, 3, 'Kādas jūras krastos atrodas Latvija?'),
(9, 3, 'Kas ir Latvijas augstākā virsotne?'),
(10, 3, 'Kurš ir lielākais ezers Latvijā?'),
(11, 3, 'Kurš ir dziļākais ezers Latvijā?'),
(12, 3, 'Kura ir pasaulē lielākā ūdenstilpne?');

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

CREATE TABLE `question_answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_answers`
--

INSERT INTO `question_answers` (`id`, `question_id`, `answer`, `correct_answer`) VALUES
(1, 1, '1', 0),
(2, 1, '2', 0),
(3, 1, '3', 0),
(4, 1, '4', 1),
(5, 2, '7', 1),
(6, 2, '4', 0),
(7, 3, '7', 0),
(8, 3, '4', 0),
(9, 3, '5', 1),
(10, 3, '2', 0),
(11, 3, '3', 0),
(12, 3, '10', 0),
(13, 4, '50', 0),
(14, 4, '40', 0),
(15, 4, '500', 0),
(16, 4, '101', 0),
(17, 4, '105', 0),
(18, 4, '1000', 0),
(19, 4, '70', 0),
(20, 4, '44', 0),
(21, 4, '100', 1),
(22, 4, '22', 0),
(23, 4, '33', 0),
(24, 5, 'Daugavpils', 0),
(25, 5, 'Rīga', 1),
(26, 5, 'Tallina', 0),
(27, 6, 'Daugavpils', 0),
(28, 6, 'Rēzekne', 0),
(29, 6, 'Valka', 0),
(30, 6, 'Liepāja', 0),
(31, 6, 'Rīga', 1),
(32, 6, 'Cēsis', 0),
(33, 6, 'Baldone', 0),
(34, 6, 'Salaspils', 0),
(35, 6, 'Kuldīga', 0),
(36, 7, 'Rīga', 0),
(37, 7, 'Grobiņa', 0),
(38, 7, 'Durbe', 1),
(39, 7, 'Aizpute', 0),
(40, 8, 'Baltijas jūras', 1),
(41, 8, 'Vidusjūras', 0),
(42, 8, 'Ziemeļu jūras', 0),
(43, 8, 'Nāves jūras', 0),
(44, 8, 'Karību jūras', 0),
(45, 9, 'Lielais Liepukalns', 0),
(46, 9, 'Dzerkaļu kalns', 0),
(47, 9, 'Gaiziņkalns', 1),
(48, 9, 'Everests', 0),
(49, 9, 'Sirdskalns', 0),
(50, 9, 'Mazais Gaiziņkalns', 0),
(51, 9, 'Dravēnu kalns', 0),
(52, 9, 'Boķu kalns', 0),
(53, 9, 'Dubuļu kalns', 0),
(54, 9, 'Nesaules kalns', 0),
(55, 10, 'Rāznas ezers', 0),
(56, 10, 'Engures ezers', 0),
(57, 10, 'Kaņieris', 0),
(58, 10, 'Burtnieks', 0),
(59, 10, 'Lubāns', 1),
(60, 11, 'Garais ezers', 0),
(61, 11, 'Lubāns', 0),
(62, 11, 'Rāznas ezers', 0),
(63, 11, 'Drīdzis', 1),
(64, 11, 'Dziļezers', 0),
(65, 11, 'Geraņimovas Ilzas ezers', 0),
(66, 12, 'Ķīšezers', 0),
(67, 12, 'Klusais okeāns', 1),
(68, 12, 'Ziemeļu ledus okeāns', 0),
(69, 12, 'Daugava', 1);

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `correct_answer_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`) VALUES
(1, 'Aritmētikas tests'),
(2, 'Pilsētu tests'),
(3, 'Ģeogrāfijas tests');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_answers`
--

CREATE TABLE `user_answers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_answers`
--
ALTER TABLE `user_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `test_id` (`test_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `answer_id` (`answer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `question_answers`
--
ALTER TABLE `question_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_answers`
--
ALTER TABLE `user_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD CONSTRAINT `question_answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `results`
--
ALTER TABLE `results`
  ADD CONSTRAINT `results_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `results_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_answers`
--
ALTER TABLE `user_answers`
  ADD CONSTRAINT `user_answers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_answers_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_answers_ibfk_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_answers_ibfk_4` FOREIGN KEY (`answer_id`) REFERENCES `question_answers` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
