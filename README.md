# Vendon testa uzdevums

Nepieciešams uzprogrammēt vienkāršu testu sistēmu, kurā lietotājs ievada savu vārdu, izvēlās testu, izpilda to, un beigās redz savu rezultātu.
Risinājumā jāizmanto PHP, MySQL, HTML, CSS, JavaScript. PHP kodam jābūt objektorientētam. Dizainiskais risinājums ir katra paša ziņā - var brīvi izpausties. 

Tests sastāv no 3 dažādiem skatiem:
Sākumlapas - lietotājs ievada savu vārdu un izvēlas kādu no datubāzē esošajiem testiem.
Testa jautājumu skats - katram jautājumam ir n atbilžu varianti. Viens no tiem ir pareizs.
Rezultāta skats - lietotājs redz savu rezultātu.


## Darba uzsākšana

Šīs instrukcijas palīdzēs sagatavot serveri darbam ar doto sistēmu.

### Nosacījumi

Lai varētu palaist doto sistēmu, būs nepieciešams serveris ar PHP un MySql atbalstu.
Šajā gadījumā, ieteiktais risinājums ir [xampp web serveris](https://www.apachefriends.org/index.html), kas satur mums nepieciešamo Apache, MariaDB, PHP.

### Servera sagatavošana

Kad serveris ir lejupielādēts un ieinstalēts, atveram XAMPP Control Panel un iedarbinām Apache un MySQL moduļus.

Visus repozitorija failus saglabājam direktorijā 
```
../xampp/htdocs/vendon_test
```

Tagad, izmantojot tīmekļa pārlūkprogrammas, varam piekļūt sistēmai vietnē
```
http://localhost/vendon_test/
```

Taču pirms darba uzsākšanas, vēl jāsakārto datubāze.

### Datubāzes sagatavošana

Šajā projektā tika izmantota *phpMyAdmin* datubāze, kurai var piekļūt 
```
http://localhost/vendon_test/
```

Pēc noklusējuma, lietotājvārdam vajadzētu būt *root* un parolei nevajadzētu būt uzstādītai.
Ja jums tomēr ir uzstādīts cits lietotājvārds un parole, izmainiet failu *db_config.php*, lai tas atbilstu jūsu iestatījumiem.

Kad tas ir izdarīts, dodamies uz *SQL* sadaļu un izveidojam datubāzi sistēmas datu glabāšanai
```
CREATE DATABASE vendon_test
```

Viss, ko atliek izdarīt, ir atvērt *Import* sadaļu izveidotajā datubāzē un importēt failu *vendon_test.sql*

Sistēmai vajadzētu būt gatavai darbam.

## Autors

* **Māris Andris Akmentiņš** - *akmentins.maris@gmail.com*
