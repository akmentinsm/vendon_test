
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Vendon test">
	<meta name="author" content="">
	<title>Home</title>

	<!-- Bootstrap core CSS -->
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<!-- Jquery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Core CSS and scripts -->
	<link rel="stylesheet" href="views/assets/css/app.css" />
	<script src="views/assets/js/app.js"></script>
</head>

<body>
	<div class="container">
		<?php include 'inc/StartController.php' ?>
	</div>
</body>
</html>